const { execSync, spawn, spawnSync } = require('child_process');
const fs = require('fs');
const path = require('path');
const http = require('http');

const npm = process.platform === 'win32' ? 'npm.cmd' : 'npm';
const dir = 'content-api';
let save = false;

if (process.argv[2] === 'save') {
  save = true;
}

function deleteFolderRecursive(folder) {
  if (fs.existsSync(folder)) {
    fs.readdirSync(folder).forEach(file => {
      const curPath = `${folder}/${file}`;
      if (fs.lstatSync(curPath).isDirectory()) {
        deleteFolderRecursive(curPath);
      } else {
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(folder);
  }
}

function copyFileSync(source, target) {
  let targetFile = target;

  if (fs.existsSync(target)) {
    if (fs.lstatSync(target).isDirectory()) {
      targetFile = path.join(target, path.basename(source));
    }
  }

  fs.writeFileSync(targetFile, fs.readFileSync(source));
}

function copyFolderRecursiveSync(source, target) {
  let files = [];

  const targetFolder = path.join(target, path.basename(source));
  if (fs.existsSync(targetFolder)) deleteFolderRecursive(targetFolder);

  if (!fs.existsSync(targetFolder)) {
    fs.mkdirSync(targetFolder);
  }

  if (fs.lstatSync(source).isDirectory()) {
    files = fs.readdirSync(source);
    files.forEach(file => {
      const curSource = path.join(source, file);
      if (fs.lstatSync(curSource).isDirectory()) {
        copyFolderRecursiveSync(curSource, targetFolder);
      } else {
        copyFileSync(curSource, targetFolder);
      }
    });
  }
}

function generate() {
  console.log('Installing packages...');
  const install = spawnSync(npm, ['install'], {
    cwd: dir
  });

  console.log('Building content API...');
  const build = spawnSync(npm, ['run', 'build'], {
    cwd: dir
  });

  console.log('Starting content API...');
  const start = spawn(npm, ['start'], {
    cwd: dir
  });

  const saveAssets = () => {
    console.log('Copying static assets into our application folder...');
    const copy = copyFolderRecursiveSync(`./${dir}/public/uploads`, './public');

    http
      .get('http://localhost:1337/locations', res => {
        const { statusCode } = res;
        const contentType = res.headers['content-type'];

        let error;
        if (statusCode !== 200) {
          error = new Error('Request Failed.\n'`Status Code: ${statusCode}`);
        } else if (!/^application\/json/.test(contentType)) {
          error = new Error(
            'Invalid content-type.\n' +
              `Expected application/json but received ${contentType}`
          );
        }
        if (error) {
          console.error(error.message);
          res.resume();
          process.exit();
        }

        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', chunk => {
          rawData += chunk;
        });
        res.on('end', () => {
          try {
            // console.log(JSON.parse(rawData));
            console.log('Saving content as JSON...');
            fs.writeFileSync('./src/data.json', rawData);

            console.log('Killing child process...');
            spawnSync('taskkill', ['/pid', start.pid, '/f', '/t']);

            if (!save) {
              console.log('Deleting the files we created ...');
              deleteFolderRecursive(dir);
            }

            console.log('Successfully generated content! Closing now...');
            process.exit();
          } catch (e) {
            console.error(e.message);
            process.exit();
          }
        });
      })
      .on('error', e => {
        console.error(`Got error: ${e.message}`);
        process.exit();
      });
  };
  setTimeout(saveAssets, 2000);
}

fs.access(dir, error => {
  if (!error) {
    generate();
  } else {
    console.log('Pulling down content repo...');
    const clone = execSync('npm run fetch', (error2, stdout, stderr) => {
      console.log(stdout);
      console.log(stderr);
      if (error !== null) {
        console.log(`exec error: ${error2}`);
        process.exit();
      }
    });
    generate();
  }
});
