import { createMuiTheme } from '@material-ui/core/styles';
import pink from '@material-ui/core/colors/pink';

const Theme = createMuiTheme({
  palette: {
    primary: { 500: '#2962ff' },
    secondary: pink
  }
});

export default Theme;
