export { default as HomePage } from './Home';
export { default as LocationsPage } from './Locations';
export { default as MobileApp } from './MobileApp';
export { default as Employment } from './Employment';
export { default as ContactPage } from './Contact';
