import React from 'react';
import { Container } from '@material-ui/core';
import { Map } from '../../components';

export default function Locations() {
  return (
    <Container maxWidth="lg" className="root">
      <Map />
    </Container>
  );
}
