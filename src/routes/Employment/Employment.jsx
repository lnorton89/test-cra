import React from 'react';
import { Container, Grid, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column'
  }
}));

export default function Employment() {
  const classes = useStyles();

  return (
    <Container maxWidth="lg" className="root">
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Typography>Employment</Typography>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}
