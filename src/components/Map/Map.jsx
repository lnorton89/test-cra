import React, { useState } from 'react';
import {
  InfoWindow,
  GoogleMap,
  useLoadScript,
  Marker
} from '@react-google-maps/api';
import { makeStyles } from '@material-ui/core/styles';
import {
  Grid,
  Paper,
  FormControlLabel,
  Slider,
  Typography
} from '@material-ui/core';
import Preloader from '../Preloader';

const data = require('../../data.json');

const useStyles = makeStyles(theme => ({
  papermap: {
    padding: theme.spacing(0),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column'
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column'
  },
  form: {
    padding: theme.spacing(3)
  }
}));

export default function Map() {
  const classes = useStyles();
  const labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let labelIndex = 0;

  const [sliderValue, setValue] = React.useState(30);
  const [mapRef, setMapRef] = useState(null);
  const [selectedPlace, setSelectedPlace] = useState(null);
  const [markerMap, setMarkerMap] = useState({});
  const [center, setCenter] = useState({ lat: 38.05, lng: -89.115601 });
  const [zoom, setZoom] = useState(5);
  const [clickedLatLng, setClickedLatLng] = useState(null);
  const [infoOpen, setInfoOpen] = useState(false);

  const handleSliderhChange = (event, newValue) => {
    setValue(newValue);
  };

  // Iterate data to size, center, and zoom map to contain all markers
  const fitBounds = map => {
    const bounds = new window.google.maps.LatLngBounds();
    data.map(place => {
      bounds.extend({ lat: place.lat, lng: place.lng });
      return place.id;
    });
    map.fitBounds(bounds);
  };

  const loadHandler = map => {
    // Store a reference to the google map instance in state
    setMapRef(map);
    // Fit map bounds to contain all markers
    fitBounds(map);
  };

  // We have to create a mapping of our places to actual Marker objects
  const markerLoadHandler = (marker, place) =>
    setMarkerMap(prevState => ({ ...prevState, [place.id]: marker }));

  const markerClickHandler = (event, place) => {
    // Remember which place was clicked
    setSelectedPlace(place);

    // Required so clicking a 2nd marker works as expected
    if (infoOpen) {
      setInfoOpen(false);
    }

    setInfoOpen(true);

    // If you want to zoom in a little on marker click
    if (zoom < 13) {
      setZoom(13);
    }

    // if you want to center the selected Marker
    // setCenter(place.pos)
  };

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: 'AIzaSyBaniHuii8SKzWg-AS11ObtqxoFXAbX6ho'
  });

  console.log(`mapref: ${mapRef}`);

  const renderMap = () => (
    <Grid container spacing={3}>
      <Grid item xs={12} md={8} lg={9}>
        <Paper className={classes.papermap}>
          <GoogleMap
            onLoad={loadHandler}
            // Save the current center position in state
            // onCenterChanged={() => setCenter(mapRef.getCenter().toJSON())}
            // Save the user's map click position
            onClick={e => setClickedLatLng(e.latLng.toJSON())}
            id="example-map"
            mapContainerStyle={{
              height: '500px',
              width: '100%'
            }}
            center={center}
            zoom={zoom}
          >
            {data.map(item => (
              <Marker
                onLoad={marker => markerLoadHandler(marker, item)}
                onClick={event => markerClickHandler(event, item)}
                onFocus
                key={`marker-${item.id}`}
                title={item.name}
                position={{
                  lat: item.lat,
                  lng: item.lng
                }}
                label={{
                  text: labels[labelIndex++],
                  color: 'white',
                  fontWeight: 'bold'
                }}
              />
            ))}
            {infoOpen && selectedPlace && (
              <InfoWindow
                anchor={markerMap[selectedPlace.id]}
                onCloseClick={() => setInfoOpen(false)}
              >
                <div>
                  <h3>{selectedPlace.name}</h3>
                  <div>This is your info window content</div>
                </div>
              </InfoWindow>
            )}
          </GoogleMap>
        </Paper>
      </Grid>
      <Grid item xs={12} md={4} lg={3}>
        <Paper className={classes.paper}>
          <Typography variant="h4" gutterBottom>
            Locations
          </Typography>
          <FormControlLabel
            className={classes.form}
            control={
              <Slider
                valueLabelDisplay="auto"
                value={sliderValue}
                onChange={handleSliderhChange}
                aria-labelledby="continuous-slider"
              />
            }
          ></FormControlLabel>
          {/* Our center position always in state */}
          <h3>
            Center {center.lat}, {center.lng}
          </h3>

          {/* Position of the user's map click */}
          {clickedLatLng && (
            <h3>
              You clicked: {clickedLatLng.lat}, {clickedLatLng.lng}
            </h3>
          )}

          {/* Position of the user's map click */}
          {selectedPlace && <h3>Selected Marker: {selectedPlace.name}</h3>}
        </Paper>
      </Grid>
    </Grid>
  );

  if (loadError) {
    return <div>Map cannot be loaded right now, sorry.</div>;
  }
  if (isLoaded) {
    return renderMap();
  }
  return <Preloader />;
}
