import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { AppBar, Tab, Tabs, Slide } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import ConfigContext from '../../config';

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    justifyContent: 'center'
  },
  scroller: {
    flexGrow: '0'
  },
  tab: {
    fontSize: '1.55em',
    fontWeight: 400,
    color: 'black',
    letterSpacing: '1.2px',
    fontFamily: 'Oswald, sans serif'
  }
}));

export default function Nav() {
  const classes = useStyles();
  const theme = useTheme();
  const history = useHistory();
  const config = useContext(ConfigContext);

  const [tabValue, setTabValue] = React.useState(0);

  React.useEffect(() => {
    if (history.location.pathname === '/') setTabValue(0);
    if (history.location.pathname === '/locations') setTabValue(1);
    if (history.location.pathname === '/mobileapp') setTabValue(2);
    if (history.location.pathname === '/employment') setTabValue(3);
    if (history.location.pathname === '/contact') setTabValue(4);
  }, [history.location.pathname]);

  const tabChange = (_event, newValue) => {
    setTabValue(newValue);
    switch (newValue) {
      case 0:
        history.push('/');
        break;
      case 1:
        history.push('locations');
        break;
      case 2:
        history.push('mobileapp');
        break;
      case 3:
        history.push('employment');
        break;
      case 4:
        history.push('contact');
        break;
      default:
        break;
    }
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Slide direction="left" timeout={1000} in mountOnEnter unmountOnExit>
          <Tabs
            classes={{ root: classes.root, scroller: classes.scroller }}
            value={tabValue}
            onChange={tabChange}
            aria-label="simple tabs example"
            textColor="primary"
            variant="scrollable"
            scrollButtons="on"
            TabIndicatorProps={{
              style: {
                backgroundColor: theme.palette.primary.main
              }
            }}
          >
            {config.routes.map((item, i) => (
              <Tab
                className={classes.tab}
                label={item}
                key={item}
                {...a11yProps(i)}
              />
            ))}
          </Tabs>
        </Slide>
      </AppBar>
    </div>
  );
}
