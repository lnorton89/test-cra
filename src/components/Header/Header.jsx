import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ConfigContext from '../../config/index';
import SocialBar from './SocialBar';
import logo from '../../images/logo.png';

const useStyles = makeStyles(() => ({
  header: {
    background: 'linear-gradient(180deg, #224ea7 0%, #133f9a 100%)',
    textAlign: 'center'
  },
  logo: {
    maxWidth: '100%',
    height: 'auto',
    margin: '0 auto',
    padding: '20px 40px'
  }
}));

export default function Header() {
  const classes = useStyles();
  const config = useContext(ConfigContext);

  return (
    <div className={classes.header}>
      <SocialBar />
      <img src={logo} alt={config.siteName} className={classes.logo} />
    </div>
  );
}
