import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Facebook } from '@material-ui/icons';
import ConfigContext from '../../../config/index';

const useStyles = makeStyles(theme => ({
  socialBar: {
    paddingTop: theme.spacing(2),
    '& > a > svg': {
      fill: '#fff',
      '&:hover': {
        fill: '#ccc'
      }
    }
  }
}));

export default function SocialBar() {
  const classes = useStyles();
  const config = useContext(ConfigContext);

  return (
    <div className={classes.socialBar}>
      <a
        href={config.facebook}
        title={`${config.siteName} Facebook`}
        target="_blank"
        rel="noopener noreferrer"
      >
        <Facebook fontSize="large" />
      </a>
    </div>
  );
}
