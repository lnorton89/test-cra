import React from 'react';
import PropTypes from 'prop-types';

export default function LocationsList(props) {
  const labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  return (
    <ul>
      {Object.keys(props.data).map((item, i) => (
        <li key={props.data[i].id}>
          {labels[i]}
          {' - '}
          {props.data[i].name}
        </li>
      ))}
    </ul>
  );
}

LocationsList.propTypes = {
  data: PropTypes.array.isRequired
};
