import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import ConfigContext from '../../config/index';

function Copyright(props) {
  return (
    <Typography variant="body2" align="center">
      Copyright © {new Date().getFullYear()} {props.siteName}.
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  footer: {
    background: 'linear-gradient(180deg, #224ea7 0%, #133f9a 100%)',
    padding: theme.spacing(6),
    color: '#fff'
  }
}));

export default function Footer() {
  const classes = useStyles();
  const config = useContext(ConfigContext);

  return (
    <footer className={classes.footer}>
      <Typography variant="subtitle1" align="center" component="p">
        Web Services by{' '}
        <Link
          href="https://www.stringer-creative.com"
          target="_blank"
          rel="noopener noreferrer"
          variant="body2"
          color="error"
        >
          Stringer Creative
        </Link>
      </Typography>
      <Copyright siteName={config.siteName} />
    </footer>
  );
}

Copyright.propTypes = {
  siteName: PropTypes.string
};
