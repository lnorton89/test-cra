import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { CircularProgress } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  preloader: {
    padding: theme.spacing(6),
    textAlign: 'center'
  }
}));

export default function Preloader(props) {
  const classes = useStyles();
  if (props.error)
    return <div className={classes.preloader}>{props.error.error}</div>;

  return (
    <div className={classes.preloader}>
      <CircularProgress />
    </div>
  );
}

Preloader.propTypes = {
  error: PropTypes.bool
};
