import React, { lazy, Suspense } from 'react';
import { Route } from 'react-router-dom';
import CacheRoute, { CacheSwitch } from 'react-router-cache-route';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { Header, Footer, Nav, Preloader } from './components';
import { HomePage } from './routes';
import { ConfigProvider } from './config';
import Theme from './theme/muiTheme';
import 'typeface-roboto';
import 'typeface-oswald';

const lazyRoutes = {
  locations: lazy(() => import('./routes/Locations')),
  mobileApp: lazy(() => import('./routes/MobileApp')),
  employment: lazy(() => import('./routes/Employment')),
  contact: lazy(() => import('./routes/Contact'))
};

function getRoutes() {
  const routes = [];

  Object.keys(lazyRoutes).forEach(key => {
    const fKey = key.charAt(0).toUpperCase() + key.slice(1);
    routes.push(fKey.replace(/([A-Z])/g, ' $1').trim());
  });

  return ['Home', ...routes];
}

const useStyles = makeStyles(theme => ({
  '@global': {
    '.root': {
      display: 'flex',
      padding: theme.spacing(2),
      minHeight: '55vh'
    }
  }
}));

function WaitingComponent(Component) {
  return props => (
    <Suspense fallback={<Preloader />}>
      <Component {...props} />
    </Suspense>
  );
}

export default function App() {
  const Config = {
    siteName: 'Wooly Wash',
    siteUrl: 'https://www.woolywash.com',
    facebook: 'https://www.facebook.com/Wooly-Wash-489444831097025',
    routes: getRoutes()
  };
  const classes = useStyles();

  return (
    <ConfigProvider value={Config} className={classes.cfg}>
      <ThemeProvider theme={Theme}>
        <CssBaseline />
        <Header />
        <Nav />
        <CacheSwitch>
          <Route path="/" exact component={HomePage} />
          {Object.keys(lazyRoutes).map(item => (
            <CacheRoute
              key={item}
              path={`/${item}`}
              component={WaitingComponent(lazyRoutes[item])}
            />
          ))}
        </CacheSwitch>
        <Footer />
      </ThemeProvider>
    </ConfigProvider>
  );
}
